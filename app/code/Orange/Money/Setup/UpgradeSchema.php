<?php

namespace Orange\Money\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function upgrade(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();
        if (version_compare($context->getVersion(), "2.2.0", "<=")) {
        //Your upgrade script
             $setup->getConnection()->addColumn(
                $setup->getTable('orangemoney_order'),
                'om_token',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'nullable' => false,
                    'after' => 'id_order',
                    'comment' => 'OM Token'
                ]
            );
            
            
        }
        $setup->endSetup();
    }
}
