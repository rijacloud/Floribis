<?php
namespace Orange\Money\Block;


class Failure extends \Magento\Framework\View\Element\Template
{
    protected $_checkoutSession;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Checkout\Model\Session $checkoutSession
        //array $data = []
    ) {
        $this->_checkoutSession = $checkoutSession;
        parent::__construct($context);
    }

    public function getRealOrderId(){
        $orderId = $this->_checkoutSession->getLastRealOrderId();
        return $orderId;
    }

    protected function _prepareLayout(){
        return parent::_prepareLayout();
    }

}
