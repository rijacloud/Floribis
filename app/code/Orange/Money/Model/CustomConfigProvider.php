<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Orange\Money\Model;
use Magento\Checkout\Model\ConfigProviderInterface;
//use Telma\Mvola\Model\PaymentMethod;
use Magento\Framework\Locale\ResolverInterface;
use Magento\Customer\Helper\Session\CurrentCustomer;
use Magento\Framework\UrlInterface;
use Magento\Payment\Helper\Data as PaymentHelper;


/**
 * Pay In Store payment method model
 */
class CustomConfigProvider implements ConfigProviderInterface
{

    public function getConfig()
    {
//        $config = [];
        $config = [
            'payment' => [
                'orangemoney' => [
                    'redirectUrl' => 'orangemoney/checkout/onepage', //onepage/saveOrder
//                    'redirectUrl' => [$this->methodCode => $this->getSaveOrderUrl()],
                ]
            ]
        ];
//        foreach ($this->methodCodes as $code) {
//            if ($this->methods[$code]->isAvailable()) {
//                $config['payment']['mvola']['redirectUrl'][$code] = $this->getMethodRedirectUrl($code);
//            }
//        }
        return $config;
    }

    /**
     * @return bool
     */
    protected function isInContextCheckout()
    {
        $this->config->setMethod(Config::OM_PAYMENT); //???

        return (bool)(int) $this->config->getValue('in_context');
    }

    /**
     * Return redirect URL for method
     *
     * @param string $code
     * @return mixed
     */
    protected function getMethodRedirectUrl($code)
    {
//        return $this->methods[$code]->getCheckoutRedirectUrl();
        return $this->methods[$code]->getBaseUrl();
    }

    /**
     * Retrieve save order url on front
     *
     * @return string
     */
    protected function getSaveOrderUrl()
    {
       //return $this->urlBuilder->getUrl('checkout/onepage/saveOrder', ['_secure' => $this->request->isSecure()]);
       return "orderUrl";
    }

}
