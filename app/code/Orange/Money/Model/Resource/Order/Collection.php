<?php
namespace Orange\Money\Model\Resource\Order;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'Orange\Money\Model\Order',
            'Orange\Money\Model\Resource\Order'
        );
    }
    
}