<?php

namespace Orange\Money\Model\Config\Source;

/**
 * Locale currency source
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Currency implements \Magento\Framework\Option\ArrayInterface
{
    protected $_options;
    protected $_options_custom;
    protected $_localeLists;

    public function __construct(\Magento\Framework\Locale\ListsInterface $localeLists)
    {
        $this->_localeLists = $localeLists;
    }
    
    public function toOptionArray()
    {        
        if (!$this->_options) {
            $this->_options = $this->_localeLists->getOptionCurrencies();
        }
        
        foreach ($this->_options as $option) {
            if (!is_array($option)) {
                continue;
            }
            if (isset($option['value']) && $option['value'] == 'MGA') {
                $this->_options_custom[] = $option;
                return $this->_options_custom;
            } else {
                continue;
            }
            
        }
        
        $options = $this->_options;
        
        return $options;
    }
}
