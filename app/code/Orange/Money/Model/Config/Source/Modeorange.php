<?php

namespace Orange\Money\Model\Config\Source;

/**
 * Used in creating options for Yes|No config value selection
 *
 */
class Modeorange 
{

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArrayMode()
    {
        return array(
            0 => __('Simulation'),
            1 => __('Production'),
        );        
    }

    /**
     * Get logoUrl options
     * @return array
     */
    public function toArrayLateralLogoUrl()
    {
        return array(
            0 => __('Externe'),
            1 => __('Local'),
        );
    }

}
