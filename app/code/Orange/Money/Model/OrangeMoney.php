<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Orange\Money\Model;



/**
 * Pay In Store payment method model
 */
class OrangeMoney extends \Magento\Payment\Model\Method\AbstractMethod
{

    /**
     * Payment code
     *
     * @var string
     */
    protected $_code = 'orangemoney';
    
    protected $_scopeConfig;
    private $checkoutSession;
    protected $storeManager;
    protected $directoryList;
    protected $_assetRepo;

    
    

    /**
     * Availability option
     *
     * @var bool
     */
    protected $_isOffline = true;
    
    protected $_urlConfig;
    
    
    
       public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Payment\Model\Method\Logger $logger,
        \Orange\Money\Helper\Data $helper,
        \Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender,
        \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\View\Asset\Repository $assetRepo,
        \Orange\Money\Model\System\Config\Source\UrlConfig $urlConfig,
//        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
//        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->_scopeConfig = $scopeConfig;
        $this->logger = $logger;
        $this->helper = $helper;
        $this->orderSender = $orderSender;
        $this->httpClientFactory = $httpClientFactory;
        $this->checkoutSession = $checkoutSession;
        $this->storeManager = $storeManager;
        $this->directoryList = $directoryList;
        $this->_assetRepo = $assetRepo;
        
        $this->_urlConfig = $urlConfig;

        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger
        );

    }
        
    /* @todo All method getConfig */
	
	public function getOMMerchantKey()
    {
        return $this->getConfigData('merchant_key');
    }

    public function getOMAuthHeader()
    {
        return $this->getConfigData('authorization');
    }

    public function getOMLogin()
    {
         return $this->getConfigData('merchant_login');
    }
	
	public function getOMAccount()
    {
         return $this->getConfigData('merchant_account');
    }
	
    public function getOMMerchantCode()
    {
         return $this->getConfigData('merchant_code');
    }
    
    public function getOMCurrency()
    {
         return $this->getConfigData('currency');
    }
    
    public function getOMReturnUrl()
    {
        $url = $this->_urlConfig->validationUrl();
         return isset($url[0]) ? $url[0] : "";
    }
    
    public function getOMCancelUrl()
    {
         $url = $this->_urlConfig->annulationUrl();
         return isset($url[0]) ? $url[0] : "";
    }
    
    public function getOMNotifUrl()
    {
        $url = $this->_urlConfig->notificationUrl();
         return isset($url[0]) ? $url[0] : "";
    }

    public function isTestMode()
    {
        return (0 == $this->getConfigData('mode'));
    }
}
