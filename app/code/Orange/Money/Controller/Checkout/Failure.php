<?php
namespace Orange\Money\Controller\Checkout;

error_reporting(E_ALL);
ini_set('display_errors', 1);


use Magento\Framework\App\Action\Context;

class Failure extends \Magento\Framework\App\Action\Action
{

    /**
     * @var \Orange\Money\Model\Factory
     */
    protected $orangemoneyFactory;
    protected $checkoutSession;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;
    
    protected $salesOrder;
    protected $storeManager;
    protected $orangemoneyOrder;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Orange\Money\Model\OrangeMoney $orangemoneyFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Sales\Model\Order $salesOrder ,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Orange\Money\Model\Order $orangemoneyOrder
    ) {
        $this->orangemoneyFactory = $orangemoneyFactory;
        $this->scopeConfig = $scopeConfig;
        $this->salesOrder = $salesOrder;
        $this->checkoutSession = $checkoutSession;
        $this->storeManager = $storeManager;
        $this->orangemoneyOrder = $orangemoneyOrder;
        parent::__construct($context);
    }
    /**
     * Order failure action
     */
    public function execute()
    {
 
        $session = $this->checkoutSession;
        $lastQuoteId = $session->getLastQuoteId();
        $lastOrderId = $session->getLastRealOrderId();

        /*if (!is_object($session) || !$lastOrderId || !$lastQuoteId) {
            $this->_redirect('checkout/cart');
            return;
        }*/

        //$order = Mage::getModel('sales/order')->loadByIncrementId($lastOrderId);
        $order = $this->salesOrder->loadByIncrementId($lastOrderId);

        $state = $transactionStatus = \Magento\Sales\Model\Order::STATE_CANCELED;

        $comment = 'Changing status to cancel';
        $order->setState($state, $transactionStatus, $comment, false);
        $order->setStatus($state, $transactionStatus, $comment, false);
        $order->save();

        $orangemoneyOrder = $this->orangemoneyOrder;
        $storeName = $this->storeManager->getStore()->getName();
        
        /*@todo orangemoney_order table : add orderId : $storeName.'_'.$lastOrderId*/
        
        $orderId = $storeName.'_'.$lastOrderId;
        $orangemoneyStatus = $orangemoneyOrder->load($orderId)->getPaymentStatus();

        if (isset($orangemoneyStatus)) {
            $orangemoneyOrder->setPaymentStatus($transactionStatus);
            $orangemoneyOrder->save();
        }
        
        $session->clearQuote();
        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }

    }
