<?php
namespace Orange\Money\Controller\Checkout;

class Success extends \Magento\Framework\App\Action\Action
{

    protected $checkoutSession;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    protected $salesOrder;
    protected $storeManager;
    
    protected $_logger;
    

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Sales\Model\Order $salesOrder ,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->salesOrder = $salesOrder;
        $this->checkoutSession = $checkoutSession;
        $this->storeManager = $storeManager;
        
        $this->_logger = $logger;
        
        parent::__construct($context);
    }

    public function execute()
    {
        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }
}
