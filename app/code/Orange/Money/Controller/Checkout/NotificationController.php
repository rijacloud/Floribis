<?php
namespace Orange\Money\Controller\Checkout;

use Orange\Money\Model;
use Orange\Money\Model\Resource;
use \Magento\Framework\App\Config\ScopeConfigInterface;

abstract class NotificationController extends \Magento\Framework\App\Action\Action
{

    protected $orangemoneyFactory;
    protected $orangemoneyOrder;
    protected $orangemoneyOrderFactory;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;
    protected $_order;
    protected $directoryList;

    protected $orderSender;
    protected $checkoutSession;
    
    protected $request;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Sales\Model\Order $order,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender,
        \Orange\Money\Model\OrangeMoney $orangemoneyFactory,
        \Orange\Money\Model\Order $orangemoneyOrder,
        \Orange\Money\Model\OrderFactory $orangemoneyOrderFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool,
        \Magento\Framework\App\RequestInterface $request
    ) {
        $this->_logger = $logger;
        $this->_order = $order;
        $this->directory_list = $directoryList;
        $this->orderSender = $orderSender;
        $this->orangemoneyFactory = $orangemoneyFactory;
        $this->orangemoneyOrder = $orangemoneyOrder;
        $this->orangemoneyOrderFactory = $orangemoneyOrderFactory;
        $this->checkoutSession = $checkoutSession;

        $this->request = $request;

        parent::__construct(
            $context
        );
        
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        
    }

    /**
     * get transaction object via WS
     * @param string $tokken
     * @return object
     */
    public function getTransaction($om_token, $order_id, $amount, $pay_token)
    {
        
        $parametres = array(
                    "order_id" => $order_id,
                    "amount" => $amount,
                    "pay_token" => $pay_token
            );
        $parametres_string = json_encode($parametres);
        
        try {
            
            $url_post = $this->orangemoneyFactory->isTestMode() ? "https://api.orange.com/orange-money-webpay/dev/v1/transactionstatus" : "https://api.orange.com/api/v1/transactionstatus";
           
            
            $curl = curl_init();

            curl_setopt_array($curl, array(
//              CURLOPT_URL => "https://api.orange.com/orange-money-webpay/dev/v1/transactionstatus",
              CURLOPT_URL => $url_post,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => $parametres_string,
              //CURLOPT_POSTFIELDS => "{\n\t\"order_id\":\"000000026\",\n\t\"amount\":105,\n\t\"pay_token\":\"e1da0dfd82b81521c5e66d138f9538a7372c3e873f31fe93d881c0eb48c93022\"\n}",
              CURLOPT_HTTPHEADER => array(
//                "Authorization: Bearer 1sNScQ64KFF9GTmGZGnLMGmXttcX",
                "Authorization: Bearer $om_token",
                "Accept: application/json",
                "Content-Type: application/json",
                "Postman-Token: 840af7c7-a85f-4da5-85ee-0668240a0831",
                "cache-control: no-cache"
              ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
              $result = null;
            } else {
              $result = json_decode($response, true);
            }
            
        } catch (Exception $e) {
            $this->_logger->debug($e->getMessage());
            $result = null;
        }

        return $result;
    }

    /**
     * notification
     */
    public function execute()
    {
        $orangeMoneyOrderFactory = $this->orangemoneyOrderFactory->create();
        $orangeMoneyOrderCollection = $orangeMoneyOrderFactory->getCollection()
                        ->addFieldToSelect('*')
                        ->addFieldToFilter("payment_status", "processing")
                        ->load();
        
        if(count($orangeMoneyOrderCollection) > 0){
            foreach ($orangeMoneyOrderCollection as $om){
                $lastOrderId = $om->getData("id_order");
                $amount = $om->getData("total_paid");
                $pay_token = $om->getData("pay_token");
                $om_token = $om->getData("om_token");

                //$idOrder = $this->_order->getId();
                $this->setOrderState($om_token, $lastOrderId, $amount, $pay_token);
            }
        }
    }
 
    public function setOrderState($om_token, $lastOrderId, $amount, $pay_token){
        
        $orangeMoneyOrder= $this->orangemoneyOrder;
        $orangeMoneyOrder->load($lastOrderId);
        $order = $this->_order->loadByIncrementId($lastOrderId);
        $idOrder = $this->_order->getId();
        
        if (!isset($idOrder) || $order->getState() == \Magento\Sales\Model\Order::STATE_CLOSED || $order->getStatus() == \Magento\Sales\Model\Order::STATE_CANCELED || $order->getStatus() == \Magento\Sales\Model\Order::STATE_COMPLETE) {
            $this->_logger->debug('Invald order status :'.$order->getState());
            return;
        }
        
        $transaction = $this->getTransaction($om_token, $lastOrderId, $amount, $pay_token);
        /*@todo $transaction notif_token vs $orangeMoneyOrder notif_token*/
        if (!isset($transaction) || empty($transaction["status"])){
            $this->_logger->debug("INFO transaction : " . json_encode($transaction));
            return;
        }
        
        /*
         * $transaction["status"] VALUE
         * INITIATED waiting for user entry
         * PENDING user has clicked on “Confirmer”, transaction is in progress on Orange side
         * EXPIRED user has clicked on “Confirmer” too late (after token’s validity)
         * SUCCESS payment is done
         * FAILED payment has failed
         */

        switch ($transaction["status"]) {
            case "INITIATED":
            case "PENDING":
                $transactionStatus = $state = \Magento\Sales\Model\Order::STATE_PROCESSING;
                break;
            case "EXPIRED":
            case "FAILED":
                $transactionStatus = $state = \Magento\Sales\Model\Order::STATE_CANCELED;
                break;
            case "SUCCESS":
                $transactionStatus = $state = \Magento\Sales\Model\Order::STATE_COMPLETE;
                break;
            default :
                $message = 'UnKnown transaction status code :'.$transaction["status"];      /* NOT FOUND */
                $this->_logger->debug($message);
                break;
        }
        
        if (!isset($transactionStatus)) {
            return;
        }
        
        $transactionStatus=4;
        $comment = 'Changing status to '.$transactionStatus;

//        $dateCde = date("Y-m-d H:i:s");
//        $order->setCreatedAt($dateCde);
        $order->setState($state, $transactionStatus, $comment, false);
        $order->setStatus($state, $transactionStatus, $comment, false);
        $order->save();

        $orangeMoneyOrder->setPaymentStatus($transaction["status"]);
        $orangeMoneyOrder->save();

//        $order->sendOrderUpdateEmail();
        $this->orderSender->send($order);
        
        return true;
    }
    
    
    
}