<?php
namespace Orange\Money\Controller\Checkout;

class Test extends \Magento\Framework\App\Action\Action
{

    protected $checkoutSession;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $orangemoneyFactory;
    
    protected $_logger;
    

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Orange\Money\Model\OrangeMoney $orangemoneyFactory,
        \Psr\Log\LoggerInterface $logger
    ) {
        
        $this->orangemoneyFactory = $orangemoneyFactory;
        $this->_logger = $logger;
        
        parent::__construct($context);
    }

    public function execute()
    {
        
        $orangemoneyFactory = $this->orangemoneyFactory;
        
        echo "<pre>";
//        print_r($orangemoneyFactory->getOMMerchantKey());
//        print_r($orangemoneyFactory->getOMAuthHeader());
//        print_r($orangemoneyFactory->getOMLogin());
//        print_r($orangemoneyFactory->getOMAccount());
//        print_r($orangemoneyFactory->getOMMerchantCode());
//        print_r($orangemoneyFactory->isTestMode());
//        print_r($orangemoneyFactory->getOMCurrency()); echo "<br />";
//        print_r($orangemoneyFactory->getOMReturnUrl()); echo "<br />";
//        print_r($orangemoneyFactory->getOMCancelUrl()); echo "<br />";
//        print_r($orangemoneyFactory->getOMNotifUrl()); echo "<br />";
        print_r($orangemoneyFactory->getOMLogin()); echo "<br />";
   
        echo "</pre>";
        
    }
}
