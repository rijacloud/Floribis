<?php
namespace Orange\Money\Controller\Checkout;

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

use Magento\Checkout\Controller\Onepage as BaseOnepage;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\App\RequestInterface;
use Magento\Customer\Model\Session;



class Onepage extends BaseOnepage
{
    
    protected $_order;
    protected $_coreRegistry = null;
    protected $_translateInline;
    protected $_formKeyValidator;
    protected $scopeConfig;
    protected $layoutFactory;
    protected $quoteRepository;
    protected $resultPageFactory;
    protected $resultLayoutFactory;
    protected $resultRawFactory;
    protected $resultJsonFactory;
    
    
    protected $customerSession;
    protected $storeManager;
    protected $_objectManager;
    protected $storeInfo;
    protected $checkoutSession;
    
    
    /**
     * Pour ajout dans table orangemoney_order
     */
    protected $orangemoneyFactory;
    protected $orangemoneyOrder;
    
    
    
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        CustomerRepositoryInterface $customerRepository,
        AccountManagementInterface $accountManagement,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\Translate\InlineInterface $translateInline,
        \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\View\LayoutFactory $layoutFactory,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Sales\Model\Order $Order,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Store\Model\Information $storeInfo,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Orange\Money\Model\OrangeMoney $orangemoneyFactory, 
        \Orange\Money\Model\Order $orangemoneyOrder
    ) {
        $this->_coreRegistry = $coreRegistry;
        $this->_translateInline = $translateInline;
        $this->_formKeyValidator = $formKeyValidator;
        $this->scopeConfig = $scopeConfig;
        $this->layoutFactory = $layoutFactory;
        $this->quoteRepository = $quoteRepository;
        $this->resultPageFactory = $resultPageFactory;
        $this->resultLayoutFactory = $resultLayoutFactory;
        $this->resultRawFactory = $resultRawFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        
        
        $this->_scopeConfig = $scopeConfig;
       
        $this->_order = $Order;
        $this->storeManager = $storeManager;
        $this->_storeInfo = $storeInfo;
        $this->checkoutSession = $checkoutSession;
        
        
        $this->orangemoneyFactory = $orangemoneyFactory;
        $this->orangemoneyOrder = $orangemoneyOrder;

        parent::__construct(
            $context,
            $customerSession,
            $customerRepository,
            $accountManagement,
            $coreRegistry,
            $translateInline,
            $formKeyValidator,
            $scopeConfig,
            $layoutFactory,
            $quoteRepository,
            $resultPageFactory,
            $resultLayoutFactory,
            $resultRawFactory,
            $resultJsonFactory
        );

    }

    public function execute(){
        $omToken = $this->getOMToken();
		
		//print_r($omToken);

		$this->saveOrderOrangeMoney($omToken); 
        
    }

	public function getOMToken() {
		
                $authorisation = $this->orangemoneyFactory->getOMAuthHeader();
            
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, "https://api.orange.com/oauth/v2/token");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials");
		curl_setopt($ch, CURLOPT_POST, 1);

		$headers = array();
		//$headers[] = "Authorization: Basic SVhhbWhsSTFrZ3ZBdDRweUdXMjl6b1ZyOTFXS0dVblQ6NUdlaThEYmlMVWxwcnVrSQ==";
//		$headers[] = "Authorization: Basic QWVabjl5NTVxNVkydkpXMmRzNUhqQmx6Yk9zZHY0bW46eUdUQUJJeWJlTm9hcmNPZQ==";
		$headers[] = "Authorization: Basic $authorisation";
		$headers[] = "Content-Type: application/x-www-form-urlencoded";
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close ($ch);
		
		$return = json_decode($result);
		return $return->access_token;
	}
	
   
        
        public function saveOrderOrangeMoney($omToken){
            $error = false;
            $result = array();
            
            try {
                
                $datenow = md5(date('DmY_His'));
                $dateCde = date("Y-m-d H:i:s");
                $order = $this->checkoutSession->getLastRealOrder();
                $orderId = $this->checkoutSession->getLastRealOrderId();
                $billing_address = $order->getBillingAddress();
                $storeName = $this->storeManager->getStore()->getName();
                $totalPaid = round($order->getBaseGrandTotal(), 2);
                $orderReference = $storeName. '_' .$orderId . '_' . $datenow;
                
                

                $order->setCreatedAt($dateCde);
                $order->save();

/*
array (
  'status' => 201,
  'message' => 'OK',
  'pay_token' => '250d6dc61dd2da9b5162804404a7e604988a57396e31f6e47e8a16fb9e530557',
  'payment_url' => 'https://webpayment-ow-sb.orange-money.com/payment/pay_token/250d6dc61dd2da9b5162804404a7e604988a57396e31f6e47e8a16fb9e530557',
  'notif_token' => '1b5a8b84d1f5793e8df7ee21200954cd',
)
*/                
                $urlRedirect = $this->getUrlRedirect($omToken, $orderId, $totalPaid);
                
                if(isset($urlRedirect) && is_array($urlRedirect)){
                    $result['redirect'] = isset($urlRedirect["payment_url"]) ? $urlRedirect["payment_url"] : "";
                    
                    $pay_token = isset($urlRedirect["pay_token"]) ? $urlRedirect["pay_token"] : "";
                    $notif_token = isset($urlRedirect["notif_token"]) ? $urlRedirect["notif_token"] : "";
                    $payment_url = isset($urlRedirect["payment_url"]) ? $urlRedirect["payment_url"] : "";
                    $payment_status = $order->getStatus();
                    
                    $id_cde = $this->insertToOrangeMoneyOrder($orderId, $omToken, $pay_token, $notif_token, $payment_url, $totalPaid, $payment_status);
                    
                }else{
                   //$result['redirect'] = "#";
                    $error = true;
                }
      
                
            } catch (Exception $e) {
                $exception = $e->getMessage();
                $error = empty($exception) ? 'Exception lors du paiement' : $exception;
            }
            
            
            if (!empty($error)) {
                $result['redirect'] = $this->checkoutHelper->getUrl('orangemoney/checkout/failure');
            }
 
            $this->getResponse()->setRedirect($result['redirect']);
        }
        
        
        
        public function getUrlRedirect($omToken, $order_id = null, $montant = null){
            
            $resultat = array();
            $orderId = isset($order_id) ? $order_id : "Test_Order_id_".mt_rand(10,99);
            $montantR = isset($montant) ? $montant : 1000;
            
            $merchant_key = $this->orangemoneyFactory->getOMMerchantKey();
            $currency = $this->orangemoneyFactory->isTestMode() ? "OUV" : $this->orangemoneyFactory->getOMCurrency();
            $return_url = $this->orangemoneyFactory->getOMReturnUrl();
            $cancel_url = $this->orangemoneyFactory->getOMCancelUrl();
            $notif_url = $this->orangemoneyFactory->getOMNotifUrl();
            $reference = $this->orangemoneyFactory->getOMLogin();
            
            $postData2 = array(
//                    "merchant_key" => "9f0545ed",
                    "merchant_key" => $merchant_key,
                    "currency" => $currency,
                    "order_id" => $orderId,
                    "amount" => $montantR,
                    "return_url" => $return_url,
                    "cancel_url" => $cancel_url,
                    "notif_url" => $notif_url,
                    "lang" => "fr",
                    "reference" => $reference
            );

        $postData2_string = json_encode($postData2);
        
//        $url_post = $this->orangemoneyFactory->isTestMode() ? "https://api.orange.com/orange-money-webpay/dev/v1/webpayment" : "https://api.orange.com/api/v1/webpayment";

		$url_post = "https://api.orange.com/orange-money-webpay/mg/v1/webpayment";
        $curl = curl_init();

		
        curl_setopt_array($curl, array(
//            CURLOPT_URL => "https://api.orange.com/orange-money-webpay/dev/v1/webpayment",
            CURLOPT_URL => $url_post,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $postData2_string,
            //CURLOPT_POSTFIELDS => "{\n\t\"merchant_key\":\"9f0545ed\",\n\t\"currency\":\"OUV\",\n\t\"order_id\":\"CDE_854241355\",\n\t\"amount\":900,\n\t\"return_url\":\"http://41.74.23.114:8081/magento/orangemoney/checkout/success/\",\n\t\"cancel_url\":\"http://41.74.23.114:8081/magento/orangemoney/checkout/failure/\",\n\t\"notif_url\":\"http://41.74.23.114:8081/magento/orangemoney/checkout/notificationcontroller/\",\n\t\"lang\":\"fr\",\n\t\"reference\":\"MerchantWP00014\"\n}",
            CURLOPT_HTTPHEADER => array(
              "Accept: application/json",
            //  "Authorization: Bearer t0wqobnsB8wBTqhVPaln8QjGg5i9",
              "Authorization: Bearer $omToken",
              "Cache-Control: no-cache",
              "Content-Type: application/json",
              "Host: api.orange.com",
              "Postman-Token: 7a3b4387-495f-4a90-9f09-e2cc81976190",
              "cache-control: no-cache"
            ),
          ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        
        
        if ($err) {
          //echo "Erreur parametrage #:" . $err;
            $resultat['error'] = $err;
        } else {
            $resultat = json_decode($response, true);
        }
        
        
        return $resultat;
        
        }
        
		
        
        public function insertToOrangeMoneyOrder(
                $id_order,
                $om_token,
                $pay_token,
                $notif_token,
                $payment_url,
                $total_paid,
                $payment_status
        ){
            $id_cde = false;
            $orangemoneyOrder = $this->orangemoneyOrder;
                $orangemoneyOrder->addData(array(
                        'id_order'          => $id_order,
                        'om_token'         => $om_token,
                        'pay_token'         => $pay_token,
                        'notif_token'       => $notif_token,
                        'payment_url'       => $payment_url,
                        'currency'          => "Ar",
                        'total_paid'        => $total_paid,
                        'payment_date'      => date("Y-m-d H:i:s"),
                        'payment_status'    => $payment_status)
                );
                
            try{    
                $id_cde = $orangemoneyOrder->save();
            }  catch (Exception $e){
                $exception = $e->getMessage();
                
            }

            return $id_cde;
        }
        

}
