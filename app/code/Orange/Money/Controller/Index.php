<?php
namespace Orange\Money\Controller\Index;
//use Magento\Framework\Controller\ResultFactory;

class Index extends \Magento\Framework\App\Action\Action 
{

    protected $request;

    /**
     * Constructor
     * 
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Request\Http $request
    )
    {
        parent::__construct($context);
        $this->request = $request;
    }

    /**
     * Execute view action
     * 
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $requestInterface = $objectManager->get('Magento\Framework\App\RequestInterface');

        $routeName = $requestInterface->getRouteName();
        $moduleName = $requestInterface->getModuleName();
        $controllerName = $requestInterface->getControllerName();
        $actionName = $requestInterface->getActionName();

        $this->_view->loadLayout();
        $this->_view->renderLayout();
//        $event = $observer->getEvent();
//        $this->redirect->redirect($event->getResponse(), 'web/app.php');
        $tokenId = $this->wsParams->mpgwTokenId;
        $url = $this->_checkout->getRedirectUrl();
        if ($tokenId && $url) {
            $this->_initToken($tokenId);
            $this->getResponse()->setRedirect($url);
            return;
        }
    }

}