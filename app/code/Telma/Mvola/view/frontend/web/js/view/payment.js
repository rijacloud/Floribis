/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
/*jshint browser:true jquery:true*/
/*global alert*/
define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'        
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
      
        rendererList.push(
            {
                type: 'mvola',
                component: 'Telma_Mvola/js/view/payment/method-renderer/mvola'
            }
        );
        
        return Component.extend({});
    }
);
