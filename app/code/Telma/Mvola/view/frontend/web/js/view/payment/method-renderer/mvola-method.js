/**
 * Copyright Â© 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*browser:true*/
/*global define*/
define(
    [
        'jquery',
        'Magento_Checkout/js/view/payment/default',
        'Magento_Checkout/js/action/place-order',
        'Magento_Checkout/js/action/select-payment-method',
        'Magento_Customer/js/model/customer',
        // 'Magento_Checkout/js/checkout-data',
        'Magento_Checkout/js/model/payment/additional-validators',
        'mage/url',
        'ko',
        'Magento_Checkout/js/model/quote',
        // 'Magento_Checkout/js/model/url-builder',
        'Magento_Checkout/js/model/full-screen-loader',
        'jquery/ui',
        // 'domReady!',

    ],
    function (
        $,
        Component,
        placeOrderAction,
        selectPaymentMethodAction,
        customer,
        additionalValidators,
        // checkoutData,
        url,
        ko,
        quote,
        flag_success,
        urlBuilder,
        fullScreenLoader

    ) {
        'use strict';
        var configValues = window.checkoutConfig;
        return Component.extend({
            defaults: {
                template: 'Telma_Mvola/payment/mvola',
                transactionResult: '',
                // afterPlaceOrderUrl: window.checkoutConfig.payment.text.redirectUrl
            },
            /**
             * Handler used by transparent
             */
            placeOrderHandler: null,
            validateHandler: null,

            /**
             * @param {Function} handler
             */
            setPlaceOrderHandler: function (handler) {
                this.placeOrderHandler = handler;
            },

            /**
             * @param {Function} handler
             */
            setValidateHandler: function (handler) {
                this.validateHandler = handler;
            },


            initialize: function () {
                this._super(); //_super will call parent's `initialize` method here
                return this;
            },

            initObservable: function () {
                this._super()
                    .observe([
                        'transactionResult'
                    ]);
                return this;
            },

            getCode: function() {
                return 'mvola';
            },

            getData: function() {
                return {
                    'method': this.item.method,
                    'additional_data': {
                        'transaction_result': this.transactionResult()
                    }
                };
            },

            getRedirectUrl: function () {
                return configValues.payment.mvola.redirectUrl[this.getCode()];
            },

            // getSaveOrderUrl: function () {
            //     return configValues.payment.mvola.saveOrderUrl[this.getCode()];
            // },

            getTransactionResults: function() {
                return _.map(configValues.payment.mvola.redirectUrl.transactionResults, function(value, key) {
                // return _.map(configValues.payment.mvola.redirectUrl[quote.paymentMethod().method], function(value, key) {
                    return {
                        'value': key,
                        'transaction_result': value
                    }
                });
            },
            // placeOrder: function () {
            //     // window.checkoutConfig.payment.mvola.redirectUrl;
            //     $.mage.redirect(
            //         configValues.payment.mvola.redirectUrl
            //     );
            //     // console.log(configValues.payment.mvola.redirectUrl)
            // },

            placeOrder: function (data, event) {
                if (event) {
                    event.preventDefault();
                }
                var self = this;
                var placeOrder = placeOrderAction(this.getData(), false, this.messageContainer);

                $.when(placeOrder).fail(function () {
                    self.isPlaceOrderActionAllowed(true);
                }).done(this.afterPlaceOrder.bind(this));
                return true;
            },

            afterPlaceOrder: function (data, event) {
                window.location.replace(url.build('mvola/checkout/onepage'));
            },

        });
    }
);