<?php
namespace Telma\Mvola\Model\Resource\Order;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'Telma\Mvola\Model\Order',
            'Telma\Mvola\Model\Resource\Order'
        );
    }
    
}