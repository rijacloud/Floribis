<?php
namespace Telma\Mvola\Model\Resource;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Order extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    
    protected function _construct()
    {
        $this->_init('mvola_order', 'id_order');
        $this->_isPkAutoIncrement = false;
    }
    
}