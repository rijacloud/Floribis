<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 
namespace Telma\Mvola\Model\System\Config\Source;

/**
 * Locale currency source
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Currency implements \Magento\Framework\Option\ArrayInterface
{
    protected $_options;
    protected $_options_custom;
    protected $_localeLists;

    public function __construct(\Magento\Framework\Locale\ListsInterface $localeLists)
    {
        $this->_localeLists = $localeLists;
    }
    
    public function toOptionArray()
    {        
        if (!$this->_options) {
            $this->_options = $this->_localeLists->getOptionCurrencies();
        }
        
        foreach ($this->_options as $option) {
            if (!is_array($option)) {
                continue;
            }
            if (isset($option['value']) && $option['value'] == 'MGA') {
                $this->_options_custom[] = $option;
                return $this->_options_custom;
            } else {
                continue;
            }
            
        }
        
        $options = $this->_options;
        
        return $options;
    }
}
