<?php
namespace Telma\Mvola\Model;


class SalesOrder extends \Magento\Sales\Model\Order
{

    /**
     * Whether specified state can be set from outside
     * @param $state
     * @return bool
     */
    public function isStateProtected($state)
    {
        if (empty($state)) {
            return false;
        }

        return parent::STATE_CLOSED == $state;
    }

}