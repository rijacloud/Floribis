<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Telma\Mvola\Model;
error_reporting(E_ALL);
ini_set('display_errors', 1);

use Magento\Framework\DataObject;
use Magento\Payment\Model\InfoInterface;
use Magento\Payment\Model\MethodInterface;
use Magento\Payment\Observer\AbstractDataAssignObserver;
use Magento\Sales\Model\Order\Invoice;
use Magento\Sales\Model\Order\Payment;
use Magento\Quote\Api\Data\PaymentMethodInterface;
use Psr\Log\LoggerInterface;
/**
 * Pay In Store payment method model
 */
class Mvola extends \Magento\Payment\Model\Method\AbstractMethod
{

    const MVOLA_PAYMENT = 'mvola';
    const MVOLA_LOGFILE = 'mvola.log';

    protected $_code = 'mvola';
    protected $_scopeConfig;
    protected $logger;

    protected $_isOffline = true;

    private $checkoutSession;
    protected $storeManager;
    protected $directoryList;
    protected $_assetRepo;


    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Payment\Model\Method\Logger $logger,
        \Telma\Mvola\Helper\Data $helper,
        \Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender,
        \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\View\Asset\Repository $assetRepo,
//        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
//        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->_scopeConfig = $scopeConfig;
        $this->logger = $logger;
        $this->helper = $helper;
        $this->orderSender = $orderSender;
        $this->httpClientFactory = $httpClientFactory;
        $this->checkoutSession = $checkoutSession;
        $this->storeManager = $storeManager;
        $this->directoryList = $directoryList;
        $this->_assetRepo = $assetRepo;

        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger
        );

    }

    public function getLogin()
    {
        return $this->getConfigData('login');
    }

    public function getpassWord()
    {
        return $this->getConfigData('password');
    }

    public function getHash()
    {
         return $this->getConfigData('hash');
    }

    public function isTestMode()
    {
        return (0 == $this->getConfigData('mode'));
    }

    public function getBaseUrl()
    {
        if ($this->isTestMode()) {
            return $this->getConfigData('urltest');
        } else {
            return $this->getConfigData('urlprod');
        }
    }

    public function getAPIVersion()
    {
         return $this->getConfigData('apiversion');
    }
    

    public function getLateralLogoUrl()
    {
        $link = '';
        $params = array('_secure' => $this->request->isSecure());
        $logoUrl = $this->assetRepo->getUrlWithParams('Telma_Mvola::images/mvola/Mvola.png', $params);

        if (!empty($link) && $this->getConfigData('logo') == 0 && @fopen($link, "r")) {
            return $link;
        } else {
            return $logoUrl;
        }
    }

    public function checkIfSaveLog()
    {
        return (1 == $this->getConfigData('log'));
    }

    public function saveLog($name, $value)
    {

        $logFile = $this->directoryList->getPath('log'). '/' .self::MVOLA_LOGFILE;
        $date = date('Y-M-d h:i:s');

        if (is_object($value) || is_array($value)) {
            if ( is_object($value) ){
                $value = (array)$value;
            }
            $message = $name . " : \n" ;
            foreach($value as $k => $v) {
                $message .= $k.' : '. strip_tags($v) . "\n";
            }
        } else {
            $message = $name.' : '. strip_tags($value);
        }

        $fp = fopen($logFile, 'a+');
        fwrite($fp, '['.$date.']: '.$message."\n");
        fclose($fp);

    }

}