<?php

/**
 * @category    Telma
 * @package     Telma_Mvola
 */

namespace Telma\Mvola\Setup;
 
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Ddl\Table;
 
class InstallSchema implements InstallSchemaInterface {
 
    public function install( SchemaSetupInterface $setup, ModuleContextInterface $context ) {
        $installer = $setup;
 
        $installer->startSetup();
 
        /**
         * Create table 'posts'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable( 'mvola_order' )
        )->addColumn(
            'id_order',
            Table::TYPE_TEXT,
            255,
            [ 'nullable' => false, 'primary' => true ],
            'Order ID'
        )->addColumn(
            'tokken',
            Table::TYPE_TEXT,
            255,
            [ 'nullable' => false ],
            'Tokken'
        )->addColumn(
            'currency',
            Table::TYPE_TEXT,
            10,
            [ 'nullable' => false ],
            'Currency'
        )->addColumn(
            'total_paid',
            Table::TYPE_TEXT,
            50,
            [ 'nullable' => false ],
            'Total Paid'
        )->addColumn(
            'payment_date',
            Table::TYPE_TEXT,
            50,
            [ 'nullable' => false ],
            'Payment Date'
        )->addColumn(
            'payment_status',
            Table::TYPE_TEXT,
            255,
            [ 'default' => NULL ],
            'Payment Status'        
        );
 
        $installer->getConnection()->createTable( $table );
 
        $installer->endSetup();
    }
}
