<?php
namespace Telma\Mvola\Block;


class About extends \Magento\Framework\View\Element\Template
{

    /**
     * @var \Telma\Mvola\Model\Factory
     */
    protected $mvolaFactory;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Telma\Mvola\Model\Mvola $mvolaFactory,
        array $data = []
    ) {
        $this->mvolaFactory = $mvolaFactory;
        parent::__construct(
            $context,
            $data
        );
    }

    public function getAboutMvolaPageUrl() {
        return 'http://www.mvola.mg/';
    }

    public function getLogoImageUrl() {
        $payment = $this->mvolaFactory;

        return $payment->getLateralLogoUrl();
    }

    protected function _prepareLayout() {
        $this->getLayout()->getBlock('head')->addCss('css/mvola.css');
    }

}
