<?php
namespace Telma\Mvola\Block;


class Failure extends \Magento\Framework\View\Element\Template
{
    protected $_checkoutSession;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Checkout\Model\Session $checkoutSession
        //array $data = []
    ) {
        $this->_checkoutSession = $checkoutSession;
        parent::__construct($context);
    }

//    public function getLogoImageUrl(){
//        return $this->_assetRepo->getUrl('Telma_Mvola::images/mvola/mvola.gif');
//    }
    public function getRealOrderId(){
//        $order = $this->_checkoutSession->getLastRealOrder();
        $orderId = $this->_checkoutSession->getLastRealOrderId();
        return $orderId;
    }

    protected function _prepareLayout(){
//        $this->getLayout()->getBlock('head')->addCss('css/mvola.css');
        return parent::_prepareLayout();
    }

}
