<?php
namespace Telma\Mvola\Block;


class Success extends \Magento\Framework\View\Element\Template
{

    protected $_checkoutSession;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Checkout\Model\Session $checkoutSession
        //array $data = []
    ) {
        $this->_checkoutSession = $checkoutSession;
        parent::__construct($context);
    }

    public function getViewFileUrl($fileId, array $params = [])
    {
        try {
            $params = array_merge(['_secure' => $this->getRequest()->isSecure()], $params);
            return $this->_assetRepo->getUrlWithParams($fileId, $params);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->_logger->critical($e);
            return $this->_getNotFoundUrl();
        }
    }

    public function getRealOrderId(){
        $orderId = $this->_checkoutSession->getLastRealOrderId();
        return $orderId;
    }
    
    

    protected function _prepareLayout(){
        return parent::_prepareLayout();
//        $this->getLayout()->getBlock('head')->addCss('css/mvola.css');
    }

}