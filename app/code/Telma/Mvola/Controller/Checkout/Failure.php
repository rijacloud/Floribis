<?php
namespace Telma\Mvola\Controller\Checkout;

use Magento\Framework\App\Action\Context;

class Failure extends \Magento\Framework\App\Action\Action
{

    /**
     * @var \Telma\Mvola\Model\Factory
     */
    protected $mvolaFactory;
    protected $checkoutSession;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;
    
    protected $salesOrder;
    protected $storeManager;
    protected $mvolaOrder;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Telma\Mvola\Model\Mvola $mvolaFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Sales\Model\Order $salesOrder ,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Telma\Mvola\Model\Order $mvolaOrder
    ) {
        $this->mvolaFactory = $mvolaFactory;
        $this->scopeConfig = $scopeConfig;
        $this->salesOrder = $salesOrder;
        $this->checkoutSession = $checkoutSession;
        $this->storeManager = $storeManager;
        $this->mvolaOrder = $mvolaOrder;
        parent::__construct($context);
    }
    /**
     * Order failure action
     */
    public function execute()
    {
        $session = $this->checkoutSession;
        $lastQuoteId = $session->getLastQuoteId();
        $lastOrderId = $session->getLastRealOrderId();

        /*if (!is_object($session) || !$lastOrderId || !$lastQuoteId) {
            $this->_redirect('checkout/cart');
            return;
        }*/

        //$order = Mage::getModel('sales/order')->loadByIncrementId($lastOrderId);
        $order = $this->salesOrder->loadByIncrementId($lastOrderId);

        $state = $transactionStatus = \Magento\Sales\Model\Order::STATE_CANCELED;

        $comment = 'Changing status to cancel';
        $order->setState($state, $transactionStatus, $comment, false);
        $order->save();

        $mvolaOrder = $this->mvolaOrder;
        $storeName = $this->storeManager->getStore()->getName();
        $orderId = $storeName.'_'.$lastOrderId;
        $mvolaStatus = $mvolaOrder->load($orderId)->getPaymentStatus();

        if (isset($mvolaStatus)) {
            $mvolaOrder->setPaymentStatus($transactionStatus);
            $mvolaOrder->save();
        }

        $session->clearQuote();
        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }

    }
