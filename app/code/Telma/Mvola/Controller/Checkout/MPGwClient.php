<?php
namespace Telma\Mvola\Controller\Checkout;

if (!extension_loaded('soap')) {
    require_once(dirname(__FILE__).'/nusoap/lib/nusoap.php');
}

/**
 * MVola payment gateway library
 *
 * @author Rija <author@example.com>
 */
class MPGwClient
{

    /**
     * MVola payment gateway object
     * @var object
     */
    private $webService;

    /**
     * web service parameters
     * @var object
     */
    private $wsParams;

    /**
     * debug: save to the log file parameters response
     * @var bool
     */
    public $debug = false;

   /**
     * log file path
     * @var string
     */
    private $logPath = '';

    /**
     * web service URL
     */
    const WS_URL = 'ws/MPGwApi';

    /**
     * transaction URL
     */
    const TRANSACTION_URL = 'transaction/';


    /**
     * Construct
     */
    public function __construct()
    {
        $this->checkSoapExtension();
        $this->wsParams = new \stdClass();
    }

    /**
     * Request payment
     * @param array $wsParameters parameters
     * 
     * @return object
     */
    public function paymentRequest($wsParameters)
    {
        $this->init($wsParameters);

        $apiVersion = $this->wsParams->API_Version;

        if (!isset($apiVersion)) {
            $this->addLog('Undefined API version');
        }

        $wsPaymentResp = $this->webService->WS_MPGw_PaymentRequest($apiVersion, $this->wsParams);
//        print_r($wsPaymentResp);die;
        if (is_array($wsPaymentResp)) {
            $wsPaymentResp = (object)$wsPaymentResp;
        }

        $this->addLog('payment response:', $wsPaymentResp);

        $this->wsParams->mpgwTokenId = (isset($wsPaymentResp->MPGw_TokenID)) ? $wsPaymentResp->MPGw_TokenID : '';

        return $wsPaymentResp;
    }

    /**
     * return transaction URL
     * @return string
     */
    public function paymentGateWayRedirect()
    {
        $tokenId = $this->wsParams->mpgwTokenId;

        if (!isset($tokenId)) {
            $this->addLog('Invalid tokken');
        }

        return $this->getTransactionUrl().$tokenId;
    }

    /**
     * Check transaction status
     * return transaction response
     * @param string $wsParameters WS parameters
     * 
     * @return  object
     */
    public function checkTransactionStatus($wsParameters)
    {
        $this->init($wsParameters);
        $this->addLog('web service parameters:', $this->wsParams);

        $apiVersion = $this->wsParams->API_Version;

        if (!isset($apiVersion)) {
            $this->addLog('Undefined API version');
        }

        $transactionStatus = $this->webService->WS_MPGw_CheckTransactionStatus($apiVersion, $this->wsParams);

        if (is_array($transactionStatus)) {
            $transactionStatus = (object)$transactionStatus;
        }

        return $transactionStatus;
    }

    /**
     * Init parameters
     * @param array $wsParameters
     */
    private function init($wsParameters)
    {
        $this->setParameters($wsParameters);
        $this->webService = new \SoapClient($this->getWebServiceUrl());
        $this->addLog('web service:', $this->webService);
    }

    /**
     * Setting parameters to wsParams
     * @param array $parameters
     */
    private function setParameters($parameters)
    {
        if (!is_array($parameters)) {
            $this->addLog('Parameters must be an array');
        }

        foreach ($parameters as $key => $value) {
            $this->wsParams->$key = $value;
        }
    }

    /**
     * return web service URL
     * @return string
     */
    private function getWebServiceUrl()
    {
        $baseUrl = $this->wsParams->BaseUrl;

        if (empty($baseUrl)) {
            $this->addLog('Undefined WS_URL');
        }

        return $baseUrl.self::WS_URL;
    }

    /**
     * return transaction URL
     * @return string
     */
    private function getTransactionUrl()
    {
        $baseUrl = $this->wsParams->BaseUrl;

        if (empty($baseUrl)) {
            $this->addLog('Undefined WS_URL');
        }

        return $baseUrl.self::TRANSACTION_URL;
    }

    /**
     * Check if SOAP is installed
     */
    private function checkSoapExtension()
    {
        if (false === class_exists("SOAPClient")) {
            $this->addLog('SOAP is not installed');
        }
    }

    /**
     * Add log message
     * @param string $message
     * @param string $value
     */
    public function addLog($message = '', $value = null)
    {
        $log = '';

        if (true == $this->debug) {
            $log = $message."\n";
            if (is_null($value)) {
                $log .= $this->arrayToString($value);
                $this->saveLog($log);
            } else {
                $this->saveLog($log);
                //throw new Exception(strip_tags($message));
            }
        } else {
            if (is_null($value)) {
                //throw new Exception(strip_tags($message));
            }
        }
    }

    /**
     * Write message log
     * @param string $message
     */
    private function saveLog($message)
    {
        if (empty($this->logPath)) {
            $logPath = dirname(__FILE__).'/mvola.log';
        } else {
            $logPath = $this->logPath;
        }

        $date = date('Y-M-d h:i:s');
        $fp = fopen($logPath, 'a+');
        fwrite($fp, '['.$date.']: '.strip_tags($message."\n"));
        fclose($fp);
    }

    /**
     * sets file destination log path
     * @param string $path
     */
    public function setLogPath($path)
    {
        $this->logPath = $path;
    }

    /**
     * Convert an array | an object values to string
     * @param mixt $value
     * 
     * @return string
     */
    private function arrayToString($value)
    {
        $return = '';

        if ((is_object($value)) || (is_array($value))) {
            foreach ($value as $k => $v) {
                $return.=$k.' : '.$v."\n";
            }
        } else {
            $return = $k.' : '.$v;
        }

        return $return;
    }

}
