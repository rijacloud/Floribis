<?php
namespace Telma\Mvola\Controller\Checkout;

class Success extends \Magento\Framework\App\Action\Action
{

    /**
     * @var \Telma\Mvola\Model\Factory
     */
    protected $mvolaFactory;
    protected $checkoutSession;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    protected $salesOrder;
    protected $storeManager;
    protected $mvolaOrder;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Telma\Mvola\Model\Mvola $mvolaFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Sales\Model\Order $salesOrder ,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Telma\Mvola\Model\Order $mvolaOrder
    ) {
        $this->mvolaFactory = $mvolaFactory;
        $this->scopeConfig = $scopeConfig;
        $this->salesOrder = $salesOrder;
        $this->checkoutSession = $checkoutSession;
        $this->storeManager = $storeManager;
        $this->mvolaOrder = $mvolaOrder;
        parent::__construct($context);
    }
    /**
     * Order failure action
     */
    public function execute()
    {

        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }
}
