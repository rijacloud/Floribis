<?php
namespace Telma\Mvola\Controller\Checkout;

/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/

use Telma\Mvola\Model;
use Telma\Mvola\Model\Resource;
use \Magento\Framework\App\Config\ScopeConfigInterface;

//require_once 'Telma/Mvola/Controller/MPGwClient.php';

abstract class NotificationController extends \Magento\Framework\App\Action\Action
{

    const PENDING = 1;
    const PROCESSING = 2;
    const TRANSACTION_CANCELED = 3;
    const COMMITTED_TRANSACTION = 4;

    /**
     * @var \Telma\Mvola\Model\Factory
     */
    protected $mvolaFactory;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;
    protected $_order;
    protected $directoryList;
    protected $mvolaOrder;
    protected $orderSender;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Telma\Mvola\Model\Mvola $mvolaFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Sales\Model\Order $order,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Telma\Mvola\Model\Order $mvolaOrder,
        \Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender
    ) {
        $this->mvolaFactory = $mvolaFactory;
        $this->_logger = $logger;
        $this->_order = $order;
        $this->directory_list = $directoryList;
        $this->mvolaOrder = $mvolaOrder;
        $this->orderSender = $orderSender;


        parent::__construct(
            $context
        );
    }

    /**
     * get transaction object via WS
     * @param string $tokken
     * @return object
     */
    public function getTransaction($tokken)
    {
        try {
            $payment = $this->mvolaFactory;

            $parameters = array(
                'Login_WS'        => $payment->getLogin(),
                'Password_WS'     => $payment->getpassWord(),
                'HashCode_WS'     => $payment->getHash(),
                'BaseUrl'         => $payment->getBaseUrl(),
                'API_Version'     => $payment->getAPIVersion(),
                'MPGw_TokenID'    => $tokken
            );


            if (empty($parameters['Login_WS']) || empty($parameters['Password_WS']) || empty($parameters['HashCode_WS'])) {
                throw new \Exception($this->module->l('incorrect web service parameters', 'validation'));
            }

            $base = $this->directory_list->getPath('app');
            $lib_file = $base.'/code/Telma/Mvola/Controller/Checkout/MPGwClient.php';
            require_once($lib_file);

            $mPGwClient = new MPGwClient();
//            $mPGwClient->setLogPath(Mage::getBaseDir('log').'/'.\Telma\Mvola\Model\paymentMethod::MVOLA_LOGFILE);
            $mPGwClient->setLogPath($this->directory_list->getPath('log').'/'.\Telma\Mvola\Model\Mvola::MVOLA_LOGFILE);
            $result = $mPGwClient->checkTransactionStatus($parameters);
        } catch (Exception $e) {
            $this->_logger->debug($e->getMessage());
            $result = null;
        }

        return $result;
    }

    /**
     * notification
     */
    public function execute()
    {
        $payment = $this->mvolaFactory;

        $transactionId = $this->getRequest()->get('Shop_TransactionID');

        $payment->saveLog('Notification: Shop_TransactionID', $transactionId);

        $mvolaOrder = $this->mvolaOrder;
        $mvolaOrder->load($transactionId);
        $tokken = $mvolaOrder->getData('tokken');

        $payment->saveLog('Notification: tokken de la transaction', $tokken);

        if (!$tokken) {
            $this->_logger->info('Undefined token with transactionId = '.$transactionId);
            return;
        }


        $_orderId = $mvolaOrder->getData('id_order');
        $orderId = substr($_orderId,strrpos($_orderId, '_')+1);

        //$order = Mage::getModel('sales/order')->loadByIncrementId($orderId);
        $order = $this->_order->loadByIncrementId($orderId);
        $idOrder = $this->_order->getId();

        if (!isset($idOrder) || $order->getState() == \Magento\Sales\Model\Order::STATE_CLOSED || $order->getStatus() == \Magento\Sales\Model\Order::STATE_CANCELED || $order->getStatus() == \Magento\Sales\Model\Order::STATE_COMPLETE) {
            $this->_logger->info('Invald status :'.$order->getState());
            return;
        }

        $transaction = $this->getTransaction($tokken);
        $payment->saveLog('Notification: rép de la transaction', $transaction);

        if (!is_object($transaction) || empty($transaction->TransactionStatusCode)) {
            $payment->saveLog('Erreur de transaction', $transaction);
            return;
        }

        switch ($transaction->TransactionStatusCode) {
            case self::PENDING:
            case self::PROCESSING:
                $transactionStatus = $state = \Magento\Sales\Model\Order::STATE_PROCESSING;
                break;
            case self::TRANSACTION_CANCELED:
                $transactionStatus = $state = \Magento\Sales\Model\Order::STATE_CANCELED;
                break;
            case self::COMMITTED_TRANSACTION:
                $transactionStatus = $state = \Magento\Sales\Model\Order::STATE_COMPLETE;
                break;
            default :
                $message = 'UnKnown transaction status code :'.$transaction->TransactionStatusCode;
                $this->_logger->info($message);
                break;
        }

        if (!isset($transactionStatus)) {
            return;
        }
        $transactionStatus=4;
        $comment = 'Changing status to '.$transactionStatus;

        $order->setState($state, $transactionStatus, $comment, false);
        $order->save();

        $mvolaOrder->setPaymentStatus($transactionStatus);
        $mvolaOrder->save();

//        $order->sendOrderUpdateEmail();
        $this->orderSender->send($order);
    }

}