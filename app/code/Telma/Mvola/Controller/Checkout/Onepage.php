<?php
namespace Telma\Mvola\Controller\Checkout;

/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/

use Magento\Checkout\Controller\Onepage as BaseOnepage;
use Magento\Checkout\Helper\Data as CheckoutHelper;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Action\Action;
use Telma\Mvola\Model;
use Telma\Mvola\Model\Resource;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\DataObject;
//use Magento\Core\Model\ObjectManager;
use Magento\Framework\App\Action\Context;
use Magento\Quote\Model\Quote\PaymentFactory;
use Magento\Framework\Model\AbstractModel;
use Magento\Store\Model\Store;
use Magento\Store\Model\Information;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
Use \Magento\Quote\Model\GuestCart\GuestCartRepository;
use Magento\Quote\Model\QuoteRepository;

use Magento\Sales\Api\Data\TransactionInterface;
use Magento\Checkout\Model\Session;

class Onepage extends BaseOnepage
{

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */

    protected $_supportedCurrencyCodes = array('MGA', 'EUR');

    protected $customerSession;

    protected $resultPageFactory;

    protected $scopeConfig;

    /**
     * @var \Telma\Mvola\Model\Factory
     */
    protected $mvolaFactory;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

//    protected $_directorylist;

    protected $salesOrder;

    protected $storeManager;

    protected $urlBuider;

    protected $checkoutHelper;

    protected $_objectManager;

    protected $_order;

    protected $mvolaOrder;

    protected $storeInfo;

    protected $jsonEncoder;

    protected $orderSender;

    protected $checkoutSession;


    public function __construct(

        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        CustomerRepositoryInterface $customerRepository,
        AccountManagementInterface $accountManagement,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\Translate\InlineInterface $translateInline,
        \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\View\LayoutFactory $layoutFactory,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
//        \Magento\Framework\ObjectManagerInterface $objectmanager,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,

        \Telma\Mvola\Model\Mvola $mvolaFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Framework\App\Filesystem\DirectoryList $_directorylist,
        \Magento\Sales\Model\Order $Order,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
//        \Magento\Framework\UrlInterface $urlBuilder,
//        CheckoutHelper $checkoutHelper,
        \Telma\Mvola\Helper\Data $checkoutHelper,
        \Telma\Mvola\Model\Order $mvolaOrder,
        \Magento\Store\Model\Information $storeInfo,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender,
//        \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory,
        \Magento\Checkout\Model\Session $checkoutSession

//        array $data = []

    ) {
        $this->_scopeConfig = $scopeConfig;
        $this->mvolaFactory = $mvolaFactory;
        $this->_logger = $logger;

//        $this->_directorylist = $_directorylist;
        $this->_Order = $Order;
        $this->storeManager = $storeManager;
//        $this->urlBuilder = $urlBuilder;
        $this->checkoutHelper = $checkoutHelper;
//        $this->_objectManager = $objectmanager;
        $this->directory_list = $directoryList;
        $this->mvolaOrder = $mvolaOrder;
        $this->_storeInfo = $storeInfo;
        $this->jsonEncoder = $jsonEncoder;
        $this->jsonHelper = $jsonHelper;
        $this->orderSender = $orderSender;
        $this->checkoutSession = $checkoutSession;
//        $this->httpClientFactory = $httpClientFactory;

        parent::__construct(
            $context,
            $customerSession,
            $customerRepository,
            $accountManagement,
            $coreRegistry,
            $translateInline,
            $formKeyValidator,
            $scopeConfig,
            $layoutFactory,
            $quoteRepository,
            $resultPageFactory,
            $resultLayoutFactory,
            $resultRawFactory,
            $resultJsonFactory,
            $directoryList
        );

    }

    public function canUseForCurrency($currencyCode) {
        if (!in_array($currencyCode, $this->_supportedCurrencyCodes)) {
            return false;
        }
        return true;
    }

    public function getStoreName()
    {
        return $this->storeManager->getStore()->getName();
    }
    public function getSaveOrderUrl()
    {
        return $this->urlBuilder->getUrl('checkout/onepage/saveOrder', ['_secure' => $this->request->isSecure()]);
    }
    /**
     * Save MVola order
     * redirect to MVola interface
     */
    public function SaveOrderMvola()
    {
        $error = false;
        $result = array();

        try {
			$datenow = md5(date('DmY_His'));
            $payment = $this->mvolaFactory;
            $login = $payment->getLogin();
            $pwd = $payment->getpassWord();
            $hash = $payment->getHash();
            $baseUrl = $payment->getBaseUrl();
            $apiVersion = $payment->getAPIVersion();

            $order = $this->checkoutSession->getLastRealOrder();
            $orderId = $this->checkoutSession->getLastRealOrderId();
            $billing_address = $order->getBillingAddress();
            $storeName = $this->storeManager->getStore()->getName();
            $totalPaid = round($order->getBaseGrandTotal(), 2);
            $orderReference = $storeName. '_' .$orderId . '_' . $datenow;
            $shopShippingName = $billing_address->getName();
            $street = is_array($billing_address->getStreet()) ? implode(" ", $billing_address->getStreet()) : $billing_address->getStreet();
            $shopShippingAddress = $street.' '.$billing_address->getCity();

            if (empty($login) || empty($pwd) || empty($hash) || empty($baseUrl) || empty($apiVersion)) {

                $error = 'MVola: Paramètrage du module incomplète';
                $this->_logger->debug($error);
            }
//            $result['redirect'] = $this->storeManager->getStore()->getUrl('mvola/checkout/onepage/failure');
            $parameters = array(
                'Login_WS'              => $login,
                'Password_WS'           => $pwd,
                'HashCode_WS'           => $hash,
                'BaseUrl'               => $baseUrl,
                'API_Version'           => $apiVersion,
                'ShopTransactionAmount' => $totalPaid,
                'ShopTransactionID'     => $orderReference,
                'ShopTransactionLabel'  => 'via ' . $storeName,
                'ShopShippingName'      => $shopShippingName,
                'ShopShippingAddress'   => $shopShippingAddress,
                'UserField1'            => '',
                'UserField2'            => '',
                'UserField3'            => ''
            );

            $base = $this->directory_list->getPath('app');
            $lib_file = $base.'/code/Telma/Mvola/Controller/Checkout/MPGwClient.php';
            require_once($lib_file);

            $mPGwClient = new MPGwClient;

            $mPGwClient->setLogPath($this->directory_list->getPath('log').'/'.\Telma\Mvola\Model\Mvola::MVOLA_LOGFILE);

            $mpgw_PaymentResp = $mPGwClient->paymentRequest($parameters);

            $payment->saveLog('Confirmation: Reponse de paiement', $mpgw_PaymentResp);

            if ( is_object($mpgw_PaymentResp) && $mpgw_PaymentResp->ResponseCode == 0 && $mpgw_PaymentResp->MPGw_TokenID != 'Error' ) {
                $mvola_order = $this->mvolaOrder;
                $mvola_order->addData(array(
                        'id_order' => $orderReference,
                        'tokken'         => $mpgw_PaymentResp->MPGw_TokenID,
                        'currency'       => $order->getQuoteCurrencyCode(),
                        'total_paid'     => $totalPaid,
                        'payment_date'   => date("Y-m-d H:i:s"),
                        'payment_status' => 'pending')
                )->save();
                //$order = Mage::getModel('sales/order')->loadByIncrementId($orderId);
                $order = $this->_Order->loadByIncrementId($orderId);

                // Sending Order Email :: oldVersion:: $order->sendNewOrderEmail();
//                $this->orderSender->send($order);

                $result['redirect'] = $mPGwClient->paymentGateWayRedirect();

//                $this->getResponse()->representJson($this->jsonEncoder->encode($result));

            } else {
                $error = $mpgw_PaymentResp->ResponseCodeDescription;
            }

        } catch(Exception $e) {
            $exception = $e->getMessage();
            $error = empty($exception) ? 'Exception lors du paiement' : $exception;
            $this->_logger->debug($exception);
        }

        if (!empty($error)) {

            $result['redirect'] = $this->checkoutHelper->getUrl('mvola/checkout/failure');

        }

        $this->getResponse()->setRedirect($result['redirect']);

    }

    public function execute()
    {
            $this->saveOrderMvola();
    }

}