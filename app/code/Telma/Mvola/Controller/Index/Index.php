<?php
namespace Telma\Mvola\Controller\Index;
use Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\ResultPageFactory;
use Magento\Framework\ObjectManager\Config\Reader\DomFactory;

class Index extends \Magento\Framework\App\Action\Action 
{

    protected $resultPageFactory;

    /**
     * Constructor
     * 
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
//        \Magento\Framework\View\LayoutFactory $layoutFactory
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * Execute view action
     * 
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
//        $blockInstance = $resultPage->getLayout()->getBlock('index.index');
        return $resultPage;

    }
}